$(function() {
    $('#twzipcode, #twzipcode2').twzipcode();
    $("#admin-form .upload").slice(0, 1).show();
    $("#admin-form .loadMore").on('click', function(e) {
        e.preventDefault();
        $("#admin-form .upload:hidden").slice(0, 1).slideDown();
        if ($("#admin-form .upload:hidden").length == 0) {
            $("#admin-form .loadMore").fadeOut('fast');
        }
    });
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    });
});