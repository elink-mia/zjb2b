$(function() {
    var footer_mb = function() {
        var mob_buyH = $('#mob-buy').innerHeight(),
            winW = $(window).width();
        if (winW < 768) {
            $('#main').css('margin-bottom', mob_buyH);
        }
    };
    $(window).on('load', function() {
        footer_mb();
    });
    $(window).resize(function() {
        footer_mb();
    });
    $('.SideBar, .icon-srh').on('click', function() {
        if ($('body').hasClass('no-scroll')) {
            $('body').removeClass("no-scroll");
        }
    });
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    });
    $('#twzipcode, #twzipcode2').twzipcode();
    $('#invoice-method').change(function() {
        $('.inv-').hide();
        $('.inv-' + $(this).val()).show("slow");
    }).change();
});