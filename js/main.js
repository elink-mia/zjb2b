(function($) {
    var dec_height = function() {
        var content = $('#page').height(),
            win_h = $(window).height(),
            main_h = $('#main').height(),
            need_h = win_h - content;
        if (content < win_h) {
            $('#main').css("min-height", main_h + need_h);
        }
    };
    var menu_mt = function() {
        var headerH = $('#header').innerHeight(),
            winW = $(window).width();
        if (winW < 992) {
            $('.offcanvas-collapse').css('top', headerH);
        }
    };
    $(window).on('load', function() {
        var winW = $(window).width();
        dec_height();
    });
    $(window).resize(function() {
        var winW = $(window).width();
        dec_height();
    });
    var burgerMenu = function() {
        $('.menu').click(function() {
            $('.offcanvas-collapse').toggleClass('open');
            if ($('.offcanvas-collapse').hasClass('open')) {
                $('.burger').addClass('active');
                $('body').addClass("no-scroll");
                $('.SideBar').on('click', function() {
                    // open sidebar
                    $('.offcanvas-collapse').removeClass('open');
                    $('.burger').removeClass('active');
                    // fade in the overlay
                    $('.overlay').fadeIn();
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            } else {
                $('.burger').removeClass('active');
                $('body').removeClass("no-scroll");
            }
        });
    };
    var sidebar = function() {
        // when opening the sidebar
        $('.SideBar').on('click', function() {
            // open sidebar
            $('#sidebar').addClass('active');
            $('body').addClass("no-scroll");
            // fade in the overlay
            $('.overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });

        // if dismiss or overlay was clicked
        $('#dismiss, .overlay').on('click', function() {
            // hide the sidebar
            $('#sidebar').removeClass('active');
            // fade out the overlay
            $('.overlay').fadeOut();
            $('body').removeClass("no-scroll");
        });
    };
    var windowScroll = function() {
        var winScroll = $(window).scrollTop() >= 1,
            test = $('#page').innerHeight(),
            winH = $(window).height(),
            headerH = $('#header').height(),
            x = test - winH;
        $(window).scroll(function() {
            if ($(window).scrollTop() >= 1 && x >= headerH) {
                $('#header').addClass('fixed');
                menu_mt();
            }
            if ($(window).scrollTop() <= 0) {
                $('#header').removeClass('fixed');
                menu_mt();
            }
        });
    };
    var hover = function() {
        var $win = $(window).width();
        if ($win > 992) {
            $(".sub-menu-parent").hover(function() {
                //Add a class of current and fade in the sub-menu
                $(this).find(".sub-menu").css({
                    'width': '120%',
                    'display': 'block',
                    'opacity': '1',
                    'z-index': '1',
                    'transform': 'translateY(0%)'
                });
            }, function() {
                $(this).find(".sub-menu").css('display', 'none');
            });
        }
        if ($win < 992) {
            $(".sub-menu-parent .nav-link").click(function() {
                if ($(this).siblings('.sub-menu').hasClass('active')) {
                    $(this).siblings('.sub-menu').removeClass('active');
                } else {
                    $('.sub-menu.active').removeClass('active');
                    $(this).siblings('.sub-menu').addClass('active');
                }
            });
        }
    };
    var goToTop = function() {
        $('.js-gotop').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $('html').offset().top
            }, 500, 'easeInOutExpo');
            return false;
        });
        $(window).scroll(function() {
            var $win = $(window);
            if ($win.scrollTop() > 200) {
                $('.js-top').addClass('active');
            } else {
                $('.js-top').removeClass('active');
            }
        });
    };
    var mob_search = function() {
        //mob版 - 商品搜尋
        $(".icon-srh, .gotosrh").click(function(e) {
            e.preventDefault();
            $("#mob-srh, #page").addClass("active");
            $('.overlay').fadeIn();
            $('body').addClass("no-scroll");
        });
        $("#srh-close, .overlay").click(function() {
            $("#mob-srh, #page").removeClass("active");
            $('.overlay').fadeOut();
            $('body').removeClass("no-scroll");
        });
    };
    $(function() {
        menu_mt();
        burgerMenu();
        sidebar();
        windowScroll();
        hover();
        goToTop();
        mob_search();
    });
})(jQuery)