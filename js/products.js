$(function() {
    var dec_height = function(first) {
        var content = $('#page').height(),
            win_h = $(window).height(),
            main_h = $('#main').height(),
            need_h = win_h - content;
        if (content < win_h || typeof first != 'undefined') {
            $('#main').css("min-height", main_h + need_h);
        }
    };
    dec_height(true);
    $('.slider-for').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        arrows: false,
        fade: true
    });
    $('.slider-nav').slick({
        dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        asNavFor: '.slider-for',
        focusOnSelect: true
    });
    $('#pro-series .list-group-item').on('click', function() {
        dec_height();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('#pro-series .list-group-item.active').removeClass('active');
            $(this).addClass('active');
        }
    });
    $(".pro-spec li").click(function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.pro-spec li.active').removeClass('active');
            $(this).addClass('active');
        }
    });
    $('.nav-tabs .nav-item').on('click', function() {
        dec_height();
    });
    //mob版 - 商品搜尋
    $("#mob-buy .btn").click(function(e) {
        e.preventDefault();
        $("#buy-select, #page").addClass("active");
        $('.overlay').fadeIn();
        $('body').addClass("no-scroll");
    });
    $("#buy-close, .overlay").click(function() {
        $("#buy-select, #page").removeClass("active");
        $('.overlay').fadeOut();
        $('body').removeClass("no-scroll");
    });
});