$(function() {
    var dec_height = function(first) {
        var content = $('#page').height(),
            win_h = $(window).height(),
            main_h = $('#main').height(),
            need_h = win_h - content;
        if (content < win_h || typeof first != 'undefined') {
            $('#main').css("min-height", main_h + need_h);
        }
    };
    dec_height(true);
    $("img.lazyload").lazyload();
    $('.ellipsis2').ellipsis({
        row: 2
    });
    $('.grid-filter li').click(function() {
        $('.grid-filter li').removeClass('active');
        $(this).addClass('active');
        $('.filter-select option').prop('selected', false);
        var tag = $(this).data('filter');
        $('.filter-select option[value="' + tag + '"]').prop('selected', true);
        history.pushState(null, null, '#' + tag);
        dec_height();
    });
    $('.filter-select').on('change', function() {
        // get filter value from option value
        var filterValue = this.value;
        filterValue = $('.filtr-item')[filterValue] || filterValue;
        //use filterFn if matches value
        filter(filterValue);
        dec_height();
    });
    $("img.lazyload").on('load', function() {
        filter();
    });
});
function filter(data) {
    if (typeof data == 'undefined') {
        $('.filtr-container').filterizr({
            layout: 'sameWidth',
            setupControls: false
        });
    } else {
        $('.filtr-container').filterizr({
            layout: 'sameWidth',
            setupControls: false,
            filter: data
        });
    }
}